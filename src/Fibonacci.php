<?php declare(strict_types=1);

namespace App;

use App\Strategy\FibonacciStrategy;

/**
 * This class is used to get a Fibonacci of nth term. It uses different strategies
 * to achieve the goal. The design follows the Strategy design pattern so that
 * you can swap your strategy at runtime
 * @package App
 */
class Fibonacci
{
    private FibonacciStrategy $fibonacciStrategy;

    /**
     * Fibonacci constructor.
     * @param FibonacciStrategy $fibonacciStrategy
     */
    public function __construct(FibonacciStrategy $fibonacciStrategy)
    {
        $this->fibonacciStrategy = $fibonacciStrategy;
    }

    /**
     * @return FibonacciStrategy
     */
    public function getFibonacciStrategy(): FibonacciStrategy
    {
        return $this->fibonacciStrategy;
    }

    /**
     * @param FibonacciStrategy $fibonacciStrategy
     */
    public function setFibonacciStrategy(FibonacciStrategy $fibonacciStrategy): void
    {
        $this->fibonacciStrategy = $fibonacciStrategy;
    }

    /**
     * Calculates the nth Fibonacci number by delegating it to
     * a strategy
     * @param int $number
     * @return float
     */
    public function getFibonacciOf(int $number): float
    {
        return $this->getFibonacciStrategy()->getNumber($number);
    }
}