<?php declare(strict_types=1);

namespace App\Strategy;

/**
 * This strategy uses matrix exponentiation to get the Fibonacci of an integer
 * The time complexity of this algorithms is O(log n)
 * This strategy uses bcmath extension for accuracy. Since the extension can
 * do big int math and for that you ensure a scientific float representation of an
 * accurate integer as a result
 */
class FibonacciMatrixExponentiationStrategy implements FibonacciStrategy
{
    /**
     * @inheritDoc
     */
    public function getNumber(int $number): float
    {
        $initialMatrix = [[1, 1], [1, 0]];

        if ($number == 0) {
            return 0;
        }

        $this->matrixToPower($initialMatrix, $number - 1);

        return (float) $initialMatrix[0][0];
    }

    /**
     * This helper function raises the matrix to a given power
     * @param array<array> $matrix
     * @param int $number
     */
    private function matrixToPower(array &$matrix, int $number): void
    {
        if( $number == 0 || $number == 1) {
            return;
        }

        $initialMatrix = [[1, 1], [1, 0]];

        $this->matrixToPower($matrix, intdiv($number, 2));
        $this->multiplyMatrix($matrix, $matrix);

        if ($number % 2 !== 0) {
            $this->multiplyMatrix($matrix, $initialMatrix);
        }
    }

    /**
     * This helper method multiplies to matrices
     * @param array<array> $firstMatrix
     * @param array<array> $secondMatrix
     */
    private function multiplyMatrix(array &$firstMatrix, array &$secondMatrix): void
    {
        $x = bcadd(
            bcmul((string) $firstMatrix[0][0], (string) $secondMatrix[0][0]),
            bcmul((string) $firstMatrix[0][1], (string) $secondMatrix[1][0])
        );
        $y = bcadd(
            bcmul((string) $firstMatrix[0][0], (string) $secondMatrix[0][1]),
            bcmul((string) $firstMatrix[0][1], (string) $secondMatrix[1][1])
        );
        $z = bcadd(
            bcmul((string) $firstMatrix[1][0], (string) $secondMatrix[0][0]),
            bcmul((string) $firstMatrix[1][1], (string) $secondMatrix[1][0])
        );
        $w = bcadd(
            bcmul((string) $firstMatrix[1][0], (string) $secondMatrix[0][1]),
            bcmul((string) $firstMatrix[1][1], (string) $secondMatrix[1][1])
        );

        $firstMatrix[0][0] = $x;
        $firstMatrix[0][1] = $y;
        $firstMatrix[1][0] = $z;
        $firstMatrix[1][1] = $w;
    }
}
