<?php declare(strict_types=1);

namespace App\Strategy;

/**
 * This interface is a strategy of solving Fibonacci numbers
 * Interface Fibonacci
 */
interface FibonacciStrategy {

    /**
     * Returns the Fibonacci of a given integer
     * @param int $number the number we need the Fibonacci for
     * @return float
     */
    public function getNumber(int $number): float;
}
