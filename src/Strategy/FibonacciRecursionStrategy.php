<?php declare(strict_types=1);

namespace App\Strategy;

/**
 * @deprecated Although this strategy can be improved by memoization
 * but it is not recommended at all due to its very slow time complexity
 * and was just introduced as a way of solving Fibonacci
 *
 * This strategy uses recursion to get the Fibonacci of an integer
 * The time complexity of O(2^n)
 */
class FibonacciRecursionStrategy implements FibonacciStrategy
{
    /**
     * @inheritDoc
     */
    public function getNumber(int $number): float
    {
        if($number <= 1) {
            return $number;
        } else {
            return $this->getNumber($number - 1) + $this->getNumber($number - 2);
        }
    }
}
