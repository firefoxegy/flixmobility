<?php declare(strict_types=1);

namespace App\Strategy;

/**
 * This strategy uses dynamic programing to get the Fibonacci of an integer
 * The time complexity of this algorithms is O(n)
 * This strategy uses bcmath extension for accuracy. Since the extension can
 * do big int math and for that you ensure a scientific float representation of an
 * accurate integer as a result
 */
class FibonacciDynamicStrategy implements FibonacciStrategy
{
    /**
     * @inheritDoc
     */
    public function getNumber(int $number): float
    {
        $previousNumber = 0;
        $nextNumber= 1;
        $temp = 0;

        for( $i = 1; $i <= $number; $i++ ) {
            $temp = bcadd((string) $nextNumber, (string) $previousNumber);
            $previousNumber = $nextNumber;
            $nextNumber = $temp;
        }

        return (float) $previousNumber;
    }
}
