.PHONY: run test api-docs clean help

build: ## run the application
	@docker run --rm -it -v ${PWD}:/app composer:2.0 install --ignore-platform-reqs

run: ## run the application
	@docker-compose up -d

test: ## run unit and functional tests
	@docker exec -it fmob_app_container php /usr/src/myapp/vendor/bin/phpunit /usr/src/myapp/tests --color=always

code-check: ## run phpstan and psalm static code analysis
	@docker exec -it fmob_app_container php ./vendor/bin/psalm
	@docker exec -it fmob_app_container php ./vendor/bin/phpstan --level=8 analyse src tests

install: ## install php dependency. EX: make install DEP=<package> DEV=--dev
	@docker run --rm -it -v ${PWD}:/app composer:2.0 require $(DEP) $(DEV) --ignore-platform-reqs

clean: ## stops the containers if exists and remove all the dependencies
	@docker exec -it fmob_app_container rm -rf vendor
	@docker-compose down --remove-orphans || true
	
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
