<?php declare(strict_types=1);

namespace App\Tests;

use App\Strategy\FibonacciMatrixExponentiationStrategy;
use PHPUnit\Framework\TestCase;
use App\Fibonacci;
use App\Strategy\FibonacciDynamicStrategy;
use App\Strategy\FibonacciRecursionStrategy;


class FibonacciTest extends TestCase
{
    private Fibonacci $fibonacci;

    public function setUp(): void
    {
        $this->fibonacci =  new Fibonacci(new FibonacciRecursionStrategy());
    }

    public function testRecursionStrategy(): void
    {
        $numbers = [15, 25];
        $expected = [610, 75025];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->fibonacci->getFibonacciOf($numbers[$i])
            );
        }
    }

    public function testDynamicStrategy(): void
    {
        $this->fibonacci->setFibonacciStrategy(new FibonacciDynamicStrategy());

        $numbers = [15, 25, 111, 120, 250, 400];
        $expected = [610, 75025, 7.049252476708912E+22, 5.358359254990966E+24, 7.89632582613173E+51,
            1.7602368064501396E+83];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->fibonacci->getFibonacciOf($numbers[$i])
            );
        }
    }

    public function testMatrixExponentiationStrategy(): void
    {
        $this->fibonacci->setFibonacciStrategy(new FibonacciMatrixExponentiationStrategy());

        $numbers = [15, 25, 111, 120, 250, 400];
        $expected = [610, 75025, 7.049252476708912E+22, 5.358359254990966E+24, 7.89632582613173E+51,
            1.7602368064501396E+83];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->fibonacci->getFibonacciOf($numbers[$i])
            );
        }
    }
}