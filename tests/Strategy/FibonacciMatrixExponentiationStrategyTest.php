<?php declare(strict_types=1);

namespace App\Tests\Strategy;

use PHPUnit\Framework\TestCase;
use App\Strategy\FibonacciMatrixExponentiationStrategy;

class FibonacciMatrixExponentiationStrategyTest extends TestCase
{
    private FibonacciMatrixExponentiationStrategy $matrixStrategy;

    public function setUp(): void
    {
        $this->matrixStrategy = new FibonacciMatrixExponentiationStrategy();
    }

    public function testGetNumber(): void
    {
        $numbers = [15, 25, 111, 120, 250, 400];
        $expected = [610, 75025, 7.049252476708912E+22, 5.358359254990966E+24, 7.89632582613173E+51,
            1.7602368064501396E+83];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->matrixStrategy->getNumber($numbers[$i])
            );
        }
    }
}