<?php declare(strict_types=1);

namespace App\Tests\Strategy;

use PHPUnit\Framework\TestCase;
use App\Strategy\FibonacciDynamicStrategy;

class FibonacciDynamicStrategyTest extends TestCase
{
    private FibonacciDynamicStrategy $dynamicStrategy;

    public function setUp(): void
    {
        $this->dynamicStrategy = new FibonacciDynamicStrategy();
    }

    public function testGetNumber(): void
    {
        $numbers = [15, 25, 111, 120, 250, 400];
        $expected = [610, 75025, 7.049252476708912E+22, 5.358359254990966E+24, 7.89632582613173E+51,
            1.7602368064501396E+83];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->dynamicStrategy->getNumber($numbers[$i])
            );
        }
    }
}