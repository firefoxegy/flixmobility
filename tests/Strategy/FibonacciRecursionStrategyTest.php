<?php declare(strict_types=1);

namespace App\Tests\Strategy;

use PHPUnit\Framework\TestCase;
use App\Strategy\FibonacciRecursionStrategy;

class FibonacciRecursionStrategyTest extends TestCase
{
    private FibonacciRecursionStrategy $recursionStrategy;

    public function setUp(): void
    {
        $this->recursionStrategy = new FibonacciRecursionStrategy();
    }

    public function testGetNumber(): void
    {
        $numbers = [15, 25];
        $expected = [610, 75025];

        for ( $i = 0, $ii = count($numbers); $i < $ii; $i++ ) {
            $this->assertEquals(
                $expected[$i],
                $this->recursionStrategy->getNumber($numbers[$i])
            );
        }
    }
}