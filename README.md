# Flixmobility

### The design process

The library designed with 3 different strategies to calculate the nth Fibonacci term. I introduced a Fibonacci class 
which has getFibonacciOf method to calculate the term. It uses different strategies based on your choice. The strategies 
designed following the Strategy design pattern so that they can get swapped at runtime.

* The recursive strategy (Not recommended at all with huge numbers due to its very slow time complexity O(2^n))
* The dynamic programing approach with a time complexity of O(n)
* The matrix exponentiation approach with a time complexity of O(log n)

### Run locally

##### Prerequisite
* `Docker` and `Docker-compose` installed on your machine

##### Run steps

```
git clone https://gitlab.com/firefoxegy/flixmobility.git
cd flixmobility
make build run
```

##### Testing and static code analysis

```
make test 
make code-check
```

##### Stop and Cleanup

```
make clean
```